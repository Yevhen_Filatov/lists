
import java.util.ArrayList;
import java.util.Collections;

public class Alist implements interface_ilist {
    private Integer[] newList;
    public Alist() {
        ArrayList <Integer> newList = new ArrayList<Integer>(10);
    }

    public Alist(int capacity) {
        newList = new Integer[changableSize(capacity)];
    }

    public Alist(int[] array) {
        newList = new Integer[changableSize(array.length)];
        for (int i = 0; i < array.length; i++) {

            newList[i] = array[i];
        }
    }

    private int changableSize(int length) {
        Integer size = Math.toIntExact(Math.round(1.2 * length));
        return size;
    }

    @Override
    public void clear() {
        newList = new Integer[0];
    }

    @Override
    public int size() {
        return newList.length;
    }

    @Override
    public int get(int index) {
        return newList[index];
    }

    @Override
    public boolean add(int element) {
        Integer[] array = new Integer[newList.length + 1];
        array[array.length - 1] = element;
        return true;
    }

    @Override
    public boolean add(int index, int number) {
        if (index > newList.length | index < 0) {
            return false;
        } else {
            newList[index] = number;
            return true;
        }
    }

    @Override
    public boolean remove(int number) {

        Integer[] array = new Integer[newList.length - 1];
        int j = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] != number) {
                array[j] = newList[i];
                j++;
            }
        }
        newList = array;
        return true;
    }

    @Override
    public boolean removeByIndex(int index) {
        if (index >= newList.length  || index < 0) {
            return false;
        } else {
            Integer[] array = new Integer[newList.length - 1];
            int j = 0;
            for (int i = 0; i < array.length; i++) {
                if (i != index) {
                    array[j] = newList[i];
                    j++;
                }
            }
            newList = array;
            return true;
        }
    }

    @Override
    public boolean contains(int number) {
        for (Integer element : newList) {
            if (element == number) {
                return true;
            }
            break;
        }
        return false;
    }

    @Override
    public void print() {
        System.out.print("[");
        for (int i = 0; i < newList.length; i++) {
            if (i < newList.length - 1) {
                System.out.print(newList[i] + ",");
            } else {
                System.out.print(newList[i]);
            }
        }
        System.out.print("]");
    }

    @Override
    public int[] toArray() {
        int[] array = new int[newList.length];
        for (int i = 0; i < newList.length; i++) {

            array[i] = newList[i];
        }
        return array;
    }

    @Override
    public interface_ilist subList(int fromIndex, int toIndex) {
        int[] subArr = new int[toIndex - fromIndex];
        int j = 0;
        for (int i = fromIndex; i < toIndex; i++) {
            subArr[j] = newList[i];
            j++;
        }
        Alist subList = new Alist(subArr);
        return subList;
    }

    @Override
    public boolean removeAll(int[] arr) {


    }

    @Override
    public boolean retainAll(int[] arr) {
        
    }
}


