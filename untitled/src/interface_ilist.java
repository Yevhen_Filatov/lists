public interface interface_ilist {


        void clear();
        int size();
        int get(int index);
        boolean add(int number);
        boolean add(int index, int number);
        boolean remove(int number);
        boolean removeByIndex(int index);
        boolean contains(int number);
        void print();//  выводит в консоль все значения
        int[] toArray(); // приводит данные к массиву
        interface_ilist subList(int fromIndex, int toIndex);
        boolean removeAll(int[] arr);
        boolean retainAll(int[] arr);
    }


